# 단감소프트 기술 블로그
단감소프트 개발팀의 기술과, 이슈를 해결한 경험을 공유하여 단감 개발팀 뿐 아니라 개발 업계 전체에 긍정적인 영향을 주기 위한 기술 블로그입니다!
## 웹사이트
[단감소프트 기술 블로그][단감소프트 기술 블로그]입니다!
## 먼저 읽어보면 좋을 내용들
- [로컬에서 Jekyll 프로젝트 구동 하는 법][로컬에서 Jekyll 프로젝트 구동하는 법]
- [Markdown으로 문서를 예쁘게 작성하는 법][Markdown으로 문서를 예쁘게 작성하는 법]

[단감소프트 기술 블로그]: https://dangam-tech.gitlab.io
[로컬에서 Jekyll 프로젝트 구동하는 법]: https://dangam-tech.gitlab.io/posts/jekyll_%ED%94%84%EB%A1%9C%EC%A0%9D%ED%8A%B8_%EA%B5%AC%EB%8F%99%ED%95%98%EB%8A%94%EB%B2%95/
[Markdown으로 문서를 예쁘게 작성하는 법]: https://dangam-tech.gitlab.io/posts/%EB%B8%94%EB%A1%9C%EA%B7%B8_%EA%B2%8C%EC%8B%9C%EB%AC%BC_%EC%9E%91%EC%84%B1%ED%95%98%EB%8A%94_%EB%B2%95/