---
title: [React Native] 구르미랑 AWS 동영상 업로드
author:
  name: 류민우
  link: https://github.com/minwoo129
date: 2022-01-26 11:15:00 +0900
categories: [React Native]
tags: [React Native, Performance]
---
# 기존 파일 업로드 flow

![스크린샷_2022-01-26_오전_10.01.20](/uploads/319286674ef4587eedd8836ed59fb0c8/스크린샷_2022-01-26_오전_10.01.20.png)

위 flow처럼, 기존의 구르미랑에서의 파일 업로드 구조는 앱, 즉 Client에서 api를 통해 파일을 전송하면 서버에서 aws에 파일을 업로드하고 업로드가 완료되면 서버에서 AWS에 저장된 파일 정보를 토대로 각종 기능들을 처리하게 된다.

하지만 동영상의 경우 파일 자체가 너무 무거워, 위 flow 처럼 서버를 먼저 통과하게 되면 최종적으로 저장이 완료되는 데 시간이 너무 오래 걸리기 때문에 위 구조와는 다른 방법으로 등록 flow를 설계해야 했다.

# 동영상 파일 업로드 flow

![스크린샷_2022-01-26_오전_10.13.59](/uploads/56716b7a5a9c49d5f430638f036db386/스크린샷_2022-01-26_오전_10.13.59.png)


기존에 서버를 통과해서 AWS에 파일을 등록하는 구조에서 Client에서 직접 aws에 동영상 파일을 등록하고 등록이 완료되면 우리 api를 통해 AWS에 등록된 동영상 정보를 서버에 저장하는 구조로 바꿔 동영상이 등록되는 시간을 줄일 수 있었다.

# 문제점

새로운 동영상 등록 flow를 통해 동영상 등록 시간은 줄였지만, 안드로이드에서 동영상 등록 중 앱이 강제종료되는 오류가 발견됬다. 강제종료되는 원인을 분석한 결과, **메모리 누수**로 확인됬다.

AWS에 동영상 파일을 등록하기 위해선 동영상 파일을 base64 형식으로 변환하고, 변환된 base64 형식 동영상을 다시 arrayflow 형식으로 변환해 AWS에 등록하게 되어있는데, 기본 10MB 이상의 동영상을 base64로 1차 변환을 하는 과정에 메모리 누수가 발생해 앱이 강제 종료되는 것으로 확인됬다.

<aside>
💡 base64, arrayflow

base64: 8비트 이진 데이터(예를 들어 실행 파일이나, ZIP 파일 등)를 문자 코드에 영향을 받지 않는 공통 ASCII 영역의 문자들로만 이루어진 일련의 문자열로 바꾸는 인코딩 방식을 가리키는 개념

arraybuffer: 자바스크립트에서 특정한 크기의 메모리에 바이너리 데이터를 저장하는 객체

</aside>

### 문제 해결 과정

1. **안드로이드 heap 메모리 확대**
    
    처음에는 최종적으로 확인된 원인이 메모리 누수였던 만큼, 안드로이드의 heap 메모리를 늘려서 해결할 수 있다고 생각했다. 그 결과, 앱이 종료되는 것은 막았지만 동영상이 base64 형식으로 변환되는 데 시간이 너무 오래 걸리는 추가 문제가 확인됬다.
    
2. AWS 등록 형식 변경
    
    이에 따라, 혹시나 AWS에 등록할 때 다른 타입으로 동영상을 전송하는 것이 가능한지 [AWS 공식문서](https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property)를 확인해본 결과, Blob 타입으로도 등록이 가능한 것으로 확인됬다.
    
    <aside>
    💡 Blob
    자바스크립트에서 이미지, 사운드, 동영상과 같은 대용량 데이터를 다루는 데 최적화된 객체 타입
    
    </aside>
    
    AWS 공식문서 중 핵심 부분
    
    ![스크린샷_2022-01-25_오후_5.29.17](/uploads/28464df8323c9f1674c51f18b729139c/스크린샷_2022-01-25_오후_5.29.17.png)
    

# 실제 적용

- **적용 전 코드**
    
    ```jsx
    import S3 from 'aws-sdk/clients/s3';
    import fs from 'react-native-fs';
    import {decode} from 'base64-arraybuffer';
    ...
    const _uploadVideo = useCallback(async (video, authUserId, currentIndex) => {
        setIsLoading(true);
    
        const s3endpoint = ...; // 생략(중요 보안 정보)
        const endPoint = config.API_PATH;
        const region = 'kr-standard';
        const access_key = ...; // 생략(중요 보안 정보)
        const secret_key = ...; // 생략(중요 보안 정보)
        const bucket = ...; // 생략(중요 보안 정보)
        const upload_directory = ...; // 생략(중요 보안 정보)
        const videoPostUrl = `${endPoint}/files/${systemId}/${serviceType}/upload/s3/post/process`;
    
        const videoObj = {
          type: video.mime,
          fileName: video.uri.substring(video.uri.lastIndexOf('/') + 1),
          uri: video.uri,
          size: video.size,
        };
    
        ... (생략)
    
    		// 동영상 변환(문제가 된 부분)
        const base64 = await fs.readFile(videoObj.uri, 'base64'); // 동영상 -> base64
    		const arrayBuffer = decode(base64); // base64 -> arraybuffer
    
    		// AWS 저장
        await s3
          .putObject({
            Bucket: bucket,
            Key: upload_directory + videoObj.fileName,
            Body: arrayBuffer,
          })
          .promise()
          .then(response => {
            ...
          })
          .catch(e => {
            ...
          })
          .finally(() => {
            ...
          });
      }, []);
    ```
    
- **적용 후 코드**
    
    ```jsx
    import S3 from 'aws-sdk/clients/s3';
    ...
    const _uploadVideo = useCallback(async (video, authUserId, currentIndex) => {
        setIsLoading(true);
    
        const s3endpoint = ...; // 생략(중요 보안 정보)
        const endPoint = config.API_PATH;
        const region = 'kr-standard';
        const access_key = ...; // 생략(중요 보안 정보)
        const secret_key = ...; // 생략(중요 보안 정보)
        const bucket = ...; // 생략(중요 보안 정보)
        const upload_directory = ...; // 생략(중요 보안 정보)
        const videoPostUrl = `${endPoint}/files/${systemId}/${serviceType}/upload/s3/post/process`;
    
        const videoObj = {
          type: video.mime,
          fileName: video.uri.substring(video.uri.lastIndexOf('/') + 1),
          uri: video.uri,
          size: video.size,
        };
    
        ... (생략)
    
    		**const videoBlob = await getVideoBlob(videoObj);**
    
    		// AWS 저장
        await s3
          .putObject({
            Bucket: bucket,
            Key: upload_directory + videoObj.fileName,
            Body: **videoBlob**,
          })
          ... (생략)
      }, []);
    
    **const getVideoBlob = (video) => {
        return new Promise((resolve, reject) => {
          fetch(video.uri)
          .then(res => res.blob())
          .then((res) => {
            resolve(res);
          })
        })
    }**
    ```
    
    getVideoBlob이라는 메서드를 새로 만들고 동영상이 저장된 file 경로를 fetch해 동영상 정보를 가져오고, 그 결과를 Blob으로 변환해서 반환하도록 한 다음 AWS에 Blob 형식으로 전송하도록 수정하였다.
    

# 결과

- 메모리 누수 발생 X
- 30MB 동영상의 경우 최종 등록까지 5초 이내로 등록 완료됨

# 앞으로

기획팀과 논의해서 반려노트에서 등록할 동영상의 최대 크기를 정하고, 그 용량이 넘어가는 동영상의 경우 등록을 제한하도록 해야 함.

[참조]

1. [https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property](https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property)
2. [https://curryyou.tistory.com/441](https://curryyou.tistory.com/441)
3. [https://heropy.blog/2019/02/28/blob/](https://heropy.blog/2019/02/28/blob/)
