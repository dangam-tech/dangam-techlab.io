---
title: 'iOS Webview에서 폰트 사이즈가 작게 나오는 이슈'
author:
  name: 박 찬영
  link: https://github.com/univdev
date: 2022-01-19 11:54:00 +0900
categories: [트러블슈팅]
tags: [React Native, Webview, Javascript, iOS, Issue]
---
# 이슈
회사에서 개발 하고 있는 소프트웨어의 웹뷰 컨텐츠 부분의 **폰트가 너무 작다는** 이슈가 제보 되었습니다.  
![관리자][admin]  
CMS에서 업로드 되었던 폰트의 정확한 사이즈는 22px로, 작은 디바이스에서 보면 텍스트 크기가 상당히 크다고 느껴야 할만한데도 불구하고  
![before][before]  
이와 같이 표시 되었습니다. 사진만 보면 적당한 크기 같다고 생각이 될 수 있지만 휴대폰같이 작은 디바이스에서 스크롤 없이 보여지는 정보량이 이렇게 방대하다면 실제 체감 크기는 훨씬 작은 상황이라고 할 수 있습니다.

## 제시한 솔루션 두 가지
처음 이 이슈가 들어왔을 때는 **"Quill CSS가 적용이 되지 않아서 스타일이 깨진거겠지~"** 라고 생각했습니다. 그래서 아래 두 가지 솔루션을 제시했습니다.
- Webview에 직접 Quill CSS를 적용하는 방법
- SSR 방식으로 컨텐츠를 뿌려주는 페이지를 하나 만들고 그 페이지로 Webview를 연결하는 방법

가장 깔끔한 해결 방안이라고 생각되는 두번째 방법을 선택하고 현재 개발 되어있는 코드를 열어보았습니다.
## 현실
그러나 실제 코드를 열어봤을 때는 이미 SSR 방식으로 컨텐츠를 뿌려주고 있던 상황이였고, 그럼에도 불구하고 폰트 크기가 적용이 되지 않은 상황이라 당황했습니다. 심지어 iOS에서만 벌어지는 이슈라고 하니까 해결하기가 더욱 어려울 것이라고도 생각했어요.  
하지만 해결 방법은 생각보다 매우 쉬웠습니다.
# 해결
```react native ios font size too small```이라는 키워드로 검색을 해보니 [저와 같은 문제를 가진 사람][참고]이 남긴 질문이 있었고, 해결 방법도 제시 되어 있었습니다.
```html
<meta name="viewport" content="width=device-width, initial-scale=1">
```
웹뷰로 불러오는 컨텐츠의 ```head``` 부분에 이 태그만 삽입하면 되는 문제였죠.  
웹을 꽤 오랜 기간 개발했고 반응형 웹도 꽤 만들어봤기 때문에 어지간한 웹 이슈는 다 알고 있다고 생각했었는데 ```meta width```를 ```device-width```로 설정해줘야 한다는 간단한 사실을 누락한 것입니다.  
![after][after]  
태그를 적용하면 이와 같이 본래 사이즈대로 출력하는 것을 확인할 수 있습니다.
# 느낀점
반응형 웹과는 다르게 Native 환경에서 동작하는 앱은 Device의 해상도 등 고려해야 할 변수가 많아서 이런 요구사항도 존재한다는 사실을 알게 되었습니다.

[before]: /assets/posts/webview_font_size.PNG
[admin]: /assets/posts/admin.png
[참고]: https://stackoverflow.com/questions/54997381/react-native-webview-for-ios-text-too-small
[after]: /assets/posts/after.PNG